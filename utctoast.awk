BEGIN{PREC="quad"}
{
  split($1, datearr, "-") ### date format year-month-day
  split($2, timearr, ":") ### time format hour:minute:second
  timedecimal=timearr[1]+(timearr[2]/60.0)+(timearr[3]/3600.0) ## decimal hours
  lat=49.0347  ## latitude in decimal degrees
  lon=20.32312 ## longitude in decimal degrees
  #############################################################################
  ###   Equation of Time (Michalsky, 1988)
  #############################################################################
  ##  This algorithm for the EoT works only for years between 1950 and 2050  ##

  ##  convert day of month to day of year
  ##  (I'm sure there's a cleverer way to do this)
  (datearr[1]==2016) ? febday=1 : febday=0  ## change/add more leap year conditions if data spans other interval than a subinterval of 2011-2019
  (datearr[2]==01) ? doty=datearr[3] : ""
  (datearr[2]==02) ? doty=datearr[3]+31 : ""
  (datearr[2]==03) ? doty=datearr[3]+59+febday : ""
  (datearr[2]==04) ? doty=datearr[3]+90+febday : ""
  (datearr[2]==05) ? doty=datearr[3]+120+febday : ""
  (datearr[2]==06) ? doty=datearr[3]+151+febday : ""
  (datearr[2]==07) ? doty=datearr[3]+181+febday : ""
  (datearr[2]==08) ? doty=datearr[3]+212+febday : ""
  (datearr[2]==09) ? doty=datearr[3]+243+febday : ""
  (datearr[2]==010) ? doty=datearr[3]+273+febday : ""
  (datearr[2]==011) ? doty=datearr[3]+304+febday : ""
  (datearr[2]==012) ? doty=datearr[3]+334+febday : ""
  delta=datearr[1]-1949 ## years
  leap=int(delta*0.25)
  ##  julian day
  jd=0.5+delta*365+leap+doty+timedecimal/24 ## days
  n=jd-18628 ## days
  ##  mean longitude
  L=280.460+0.9856474*n ## degrees
  while (L>=360) {L-=360} ## keeps value between 0 and 360, may rewrite to a
  while (L<0)    {L+=360} ## faster approach later
  ##  mean anomaly
  g=357.528+0.9856003*n ## degrees
  while (g>=360) {g-=360}
  while (g<0)    {g+=360}
  ##  ecliptic longitude
  l=L+1.915*sin(g*0.01745329)+0.02*sin(2*g*0.01745329) ## degrees
  while (l>=360) {l-=360}
  while (l<0)    {l+=360}
  ##  obliguity of the ecliptic
  ep=23.439-0.0000004*n ## degrees
  ##  right ascension
  numer=cos(ep*0.01745329)*sin(l*0.01745329)
  denom=cos(l*0.01745329)
  ra=57.295779513*atan2(numer,denom) ## degrees
  (ra<0) ? ra+=360 : ""
  ## declination
  numer2=sin(ep*0.01745329)*sin(l*0.01745329)
  dec=57.295779513*atan2(numer2, 1) ## degrees
  (dec<0) ? dec+=360 : ""
  ## Greenwich mean sidereal time
  gmst=6.697375 + 0.0657098242*n + timedecimal ## hours
  ## Local mst
  while (gmst>=24) {gmst-=24}
  while (gmst<0)   {gmst+=24}
  lmst=gmst+lon/15 ## hours
  while (lmst>=24) {lmst-=24}
  while (lmst<0)   {lmst+=24}
  ## hour angle
  ha=lmst*15-ra ## degrees
  while (ha>=180) {ha-=360}
  while (ha<-180)   {ha+=360}
  ## sine of elevation = cosine of solar zenith angle
  cossza=sin(dec*0.01745329)*sin(lat*0.01745329)+cos(dec*0.01745329)*cos(lat*0.01745329)*cos(ha*0.01745329) ## unitless (I hope)
  ## note that this is not refraction-corrected

  ##  and finally, Equation of Time  ##
  eot=L-ra ## degrees                ##
  #####################################

  #############################################################################
  ###   AST calculation - this is the important bit
  #############################################################################
  ##    20.32323 is longitude in decimal degrees                             ##
  ##    so far this program works only for positive (east) longitudes        ##

  timedecimal2=timedecimal+eot/15+(lon/15)

  #############################################################################
  ###   conversion of output from decimal hours to human readable
  #############################################################################
  ##    if the new time is under 24 hours, print old date and new time       ##

  if (0 <= timedecimal2 && timedecimal2 < 24)
  {
    timearr2[1]=int(timedecimal2)
    timearr2[2]=int((timedecimal2-timearr2[1])*60)
    timearr2[3]=int((((timedecimal2-int(timedecimal2))*60)-timearr2[2])*60)
    $1=$2=""
    printf("%02i.%02i.%02i %02i:%02i:%02i %s %1.8f\n", datearr[1],datearr[2],datearr[3],timearr2[1],timearr2[2],timearr2[3],$0,cossza)
  }
  #############################################################################
  ##    if the new time is over 24 hours, fix the date & time, print it      ##
  else if (timedecimal2 > 24)
  {
    ###########################################################################
    #   determine the number of days in the current month                     #
    if (datearr[2] == 02)
      {
        days=28+febday
      }
    else if (datearr[2] == 04 || datearr[2] == 06 || datearr[2] == 09 || datearr[2] == 11)
      {
        days=30
      }
    else if (datearr[2] == 01 || datearr[2] == 03 || datearr[2] == 05 || datearr[2] == 07 || datearr[2] == 08 || datearr[2] == 10 || datearr[2] == 12)
      {
        days=31
      }
    else
      {
        print "ERROR IN DATA 1 - INVALID MONTH"
        exit
      }
    ## if new time doesn't cross into a new month, progress only day
    if (datearr[3]<days)
    {
      datearr2[1]=datearr[1]
      datearr2[2]=datearr[2]
      datearr2[3]=datearr[3]+1
    }
    ## if new time does cross into a new month
    else if (datearr[3]>=days)
    {
      ### if new time doesn't cross into a new year, progress only month & day
      if (datearr[2]<12 && datearr[2]>0)
      {
        datearr2[1]=datearr[1]
        datearr2[2]=datearr[2]+1
        datearr2[3]=datearr[3]-days+1
      }
      ### if new time does cross into a new year, progress year, month & day
      else if (datearr[2]=12)
      {
        datearr2[1]=datearr[1]+1
        datearr2[2]=datearr[2]-11
        datearr2[3]=datearr[3]-days+1
      }
      else
        {
          print "ERROR IN DATA 2 - INVALID MONTH"
          exit
        }
    }
    else
      {
        print "ERROR IN DATA 3 - INVALID DAY"
        exit
      }
    timearr2[1]=int(timedecimal2-24)
    timearr2[2]=int((timedecimal2-24-timearr2[1])*60)
    timearr2[3]=int((((timedecimal2-24-int(timedecimal2-24))*60)-timearr2[2])*60)
    $1=$2=""
    printf("%02i.%02i.%02i %02i:%02i:%02i %s %1.8f\n", datearr2[1],datearr2[2],datearr2[3],timearr2[1],timearr2[2],timearr2[3],$0,cossza)
  }
}
###############################################################################
# Algorithm:
#
# Michalsky, Joseph J. (1988). „The Astronomical Almanac’s algorithm for app-
# roximate solar position (1950–2050)“. In: Solar Energy 40 (3).
###############################################################################
