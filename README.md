# utc-to-ast

written in AWK to convert data from UTC (coordinated universal time) to AST (apparent solar time), currently hardcoded for input with lines starting with YY-MM-DD HH:mm:ss, while leaving the rest of each line intact. Doesn't work with negative longitudes yet - the decimal time will be mostly correct (as long as the new time isn't negative), but won't be correctly interpreted into human readable time.

The program is based on the Astronomical Almanac algorithm, published in Michalsky, 1988 (citation in code)

Read the comments in the code to adapt the script to your use (or adapt the data to the script, whichever you fancy), especially if your time and date format is different, or your time range includes other leap years than 2016.